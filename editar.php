<?php 
require 'config.php';

$info = [];
 $id = filter_input(INPUT_GET, 'id');
 if($id) {

    $sql = $pdo->prepare("SELECT * FROM usuarios  WHERE id = :id");
    $sql->bindvalue(':id', $id);
    $sql->execute();

    if($sql->rowcount() > 0) {

        $info = $sql->fetch( PDO::FETCH_ASSOC );

    } else {
      header("location: index.php");
      exit;
    }

 }else {
    header("location: index.php");
    exit;
 }

?>

<h1>Editar USÚARIO</h1>
<form method="POST" action="editar_action.php?id=<?=$info['id'];?>">
     <label>
        nome:<br/>
        <input type="text" name="name" value="<?=$info['nome'];?>" />
     </label><br/><br/>

     <label>
        E-mail:<br/>
        <input type="text" name="email" value="<?=$info['email'];?>" />
     </label><br/><br/>

     <input type="submit" value="editar" />
</form>